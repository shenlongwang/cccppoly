//Author: Alexander G. Schwing
#include <vector>
#include <omp.h>
#include "CostFunction.h"
#include "Polynomial.h"
template <class T>
class CostFunctionCCCPRegistration : public CostFunction<T> {
	size_t dim, nfactor;
	std::vector<T> nodeX;
	std::vector<T> nodeY;
	std::vector<T> nodeZ;
	std::vector<size_t> nodeInd1;
	std::vector<size_t> nodeInd2;
	std::vector<T> dist;
	std::vector<T> gradient;
public:
	CostFunctionCCCPRegistration(std::string filename) {
		std::ifstream polynomialFile(filename, std::ios_base::in);
		if (!polynomialFile.is_open())
			std::cerr << "Can't open data file" << std::endl;
		dim = NUM_POINTS;
		nfactor = NUM_EDGES;
		nodeX.resize(dim);
		nodeY.resize(dim);
		nodeZ.resize(dim);
		nodeInd1.resize(nfactor);
		nodeInd1 = { 1, 1, 2, 2, 2, 3, 3, 4, 4, 5, 5, 5, 6, 6, 7, 8 };
		nodeInd2.resize(nfactor);
		nodeInd2 = { 2, 4, 3, 4, 5, 5, 6, 5, 7, 6, 7, 8, 8, 9, 8, 9 };
		dist.resize(nfactor);
		dist = { 50.0, 50.0, 50.0, T(50.0*sqrt(2.0)), 50.0, T(50.0*sqrt(2.0)), 50.0, 50.0, 50.0, 50.0, T(50.0*sqrt(2.0)), 50.0, T(50.0*sqrt(2.0)), 50.0, 50.0, 50.0 };
		for (size_t k = 0; k < dim; k++)
		{
			polynomialFile >> nodeX[k] >> nodeY[k] >> nodeZ[k];
		}
		gradient.assign(dim, T(0.0));
	};

	CostFunctionCCCPRegistration(std::string filename, std::string edgefilename) {
		std::ifstream polynomialFile(filename, std::ios_base::in);
		if (!polynomialFile.is_open())
			std::cerr << "Can't open data file" << std::endl;
		std::ifstream edgeFile(edgefilename, std::ios_base::in);
		if (!edgeFile.is_open())
			std::cerr << "Can't open edge file" << std::endl;
		dim = 81;
		nfactor = 208;
		nodeX.assign(dim, T(0.0));
		nodeY.assign(dim, T(0.0));
		nodeZ.assign(dim, T(0.0));
		nodeInd1.assign(nfactor, size_t(0));
		nodeInd2.resize(nfactor, size_t(0));
		dist.resize(nfactor, T(1.0));

		for (size_t k = 0; k < nfactor; k++)
		{
			edgeFile >> nodeInd1[k] >> nodeInd2[k] >> dist[k];
			dist[k] = dist[k];
		}
		for (size_t k = 0; k < dim; k++)
		{
			polynomialFile >> nodeX[k] >> nodeY[k] >> nodeZ[k];
		}
	};
	size_t SetGradient(std::vector<T>& x) {
		T temp = T(0.0);
		size_t ind1 = 0;
		size_t ind2 = 0;
		T diff1 = T(0.0);
		T diff2 = T(0.0);
		T diff3 = T(0.0);
		gradient.assign(dim, T(0.0));
		for (size_t k = 0; k<nfactor; ++k) {
			temp = T(0.0);
			ind1 = nodeInd1[k] - 1;
			ind2 = nodeInd2[k] - 1;
			diff1 = x[ind1] * nodeX[ind1] - x[ind2] * nodeX[ind2];
			diff2 = x[ind1] * nodeY[ind1] - x[ind2] * nodeY[ind2];
			diff3 = x[ind1] * nodeZ[ind1] - x[ind2] * nodeZ[ind2];
			gradient[ind1] -= 4 * pow(dist[k], 2) * (nodeX[ind1] * diff1 + nodeY[ind1] * diff2 + nodeZ[ind1] * diff3);
			gradient[ind2] += 4 * pow(dist[k], 2) * (nodeX[ind2] * diff1 + nodeY[ind2] * diff2 + nodeZ[ind2] * diff3);
		}
		return 0;
	};
	~CostFunctionCCCPRegistration(){ };
	size_t Dimensions() { return dim; }
	T Evaluate(std::vector<T>& x, std::vector<T>& g, size_t) {
		T f = T(0.0);
		g.assign(dim, T(0.0));
		//T temp = T(0.0);
		//size_t ind1 = 0;
		//size_t ind2 = 0;
		//T diff1 = T(0.0);
		//T diff2 = T(0.0);
		//T diff3 = T(0.0);
		//T sqdiff1 = T(0.0);
		//T sqdiff2 = T(0.0);
		//T sqdiff3 = T(0.0);
		//T g1 = T(0.0);
		//T g2 = T(0.0);
#pragma omp parallel  
		{
#pragma omp for nowait
			for (int k = 0; k < nfactor; ++k) {
				T temp = T(0.0);
				T g1 = T(0.0);
				T g2 = T(0.0);
				size_t ind1 = nodeInd1[k] - 1;
				size_t ind2 = nodeInd2[k] - 1;
				T diff1 = x[ind1] * nodeX[ind1] - x[ind2] * nodeX[ind2];
				T diff2 = x[ind1] * nodeY[ind1] - x[ind2] * nodeY[ind2];
				T diff3 = x[ind1] * nodeZ[ind1] - x[ind2] * nodeZ[ind2];
				 
				T sqdiff1 = diff1*diff1;
				T sqdiff2 = diff2*diff2;
				T sqdiff3 = diff3*diff3;
				temp += sqdiff1*sqdiff1 + sqdiff2*sqdiff2 + sqdiff3*sqdiff3 - dist[k] * dist[k] * dist[k] * dist[k];
				
		/*		T sqdiff1 = pow(diff1, 2);
				T sqdiff2 = pow(diff2, 2);
				T sqdiff3 = pow(diff3, 2);
				temp += pow(sqdiff1, 2) + pow(sqdiff2, 2) + pow(sqdiff3, 2) - pow(dist[k], 4);*/
				temp += 2 * sqdiff1*sqdiff2 + 2 * sqdiff2*sqdiff3 + 2 * sqdiff3*sqdiff1;
				g1 = 4 * (nodeX[ind1] * diff1*sqdiff1 + nodeY[ind1] * diff2*sqdiff2 + nodeZ[ind1] * diff3*sqdiff3) * 0.5 / T(nfactor);
				g1 += 4 * (nodeX[ind1] * diff1*(sqdiff2 + sqdiff3) + nodeY[ind1] * diff2*(sqdiff1 + sqdiff3) + nodeZ[ind1] * diff3*(sqdiff1 + sqdiff2)) * 0.5 / T(nfactor);
				g2 = -4 * (nodeX[ind2] * diff1*sqdiff1 + nodeY[ind2] * diff2*sqdiff2 + nodeZ[ind2] * diff3*sqdiff3) * 0.5 / T(nfactor);
				g2 -= 4 * (nodeX[ind2] * diff1*(sqdiff2 + sqdiff3) + nodeY[ind2] * diff2*(sqdiff1 + sqdiff3) + nodeZ[ind2] * diff3*(sqdiff1 + sqdiff2)) * 0.5 / T(nfactor);
#pragma omp critical
				{
					f += temp;
					g[ind1] += g1;
					g[ind2] += g2;
				}
			}
		}
		f = 0.5 * f / T(nfactor);

		for (size_t i = 0; i < dim; i++)
		{
			f += gradient[i] * x[i] * 0.5 / T(nfactor);
			g[i] += gradient[i] * 0.5 / T(nfactor);
		}
		return f;
	}
	void IterationOutput(std::vector<T>&, std::vector<T>&, size_t) {}
};

template class CostFunctionCCCPRegistration<double>;
template class CostFunctionCCCPRegistration<float>;
