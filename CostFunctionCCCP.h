//Author: Alexander G. Schwing
#include <vector>

#include "CostFunction.h"
#include "Polynomial.h"
template <class T>
class CostFunctionCCCP : public CostFunction<T> {
	Polynomial<T> polynomial;
	std::vector<T> gradient;
	size_t dim;
public:
	CostFunctionCCCP(Polynomial<T> & convexPolynomial, std::vector<T>& concaveGradient) {
		polynomial = convexPolynomial;
		gradient = concaveGradient;
		if (gradient.size() != polynomial.getDim())
			std::cerr << "Dimensions of convex and concave parts are not equal";
		dim = polynomial.getDim();
	};
	CostFunctionCCCP(std::vector<T>& c, size_t deg, size_t
		d) {
		dim = d;
	};
	~CostFunctionCCCP(){ };
	size_t Dimensions() { return dim; }
	T Evaluate(std::vector<T>& x, std::vector<T>& g, size_t) {
		T f = polynomial.EvaluatePolynomial(x);
		polynomial.GradientPolynomial(x, g);
		for (size_t i = 0; i < dim; i++)
		{
			f += gradient[i] * x[i];
			g[i] += gradient[i];
		}
		return f;
	}
	void IterationOutput(std::vector<T>&, std::vector<T>&, size_t) {}
};

template class CostFunctionCCCP<double>;
template class CostFunctionCCCP<float>;
