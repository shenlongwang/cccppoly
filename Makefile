CC	= g++
CFLAGS	= -std=c++0x -pedantic -W -Wall -fopenmp -O3
EIGEN_HOME = /Users/shenlongwang/Documents/CppCode/

all:
	make polynomial 

clean:
	rm -f *.o
	rm -f polynomial 

polynomial: ArmijoLineSearch.cpp ArmijoLineSearch.h WolfeLineSearch.cpp WolfeLineSearch.h CostFunction.h CostFunctionRosenbrock.h Optimizer.cpp Optimizer.h Polynomial.h Polynomial.cpp
	$(CC) Polynomial.cpp Optimizer.cpp ArmijoLineSearch.cpp WolfeLineSearch.cpp $(CFLAGS) -o polynomial -I$(EIGEN_HOME)
