//Author: Alexander G. Schwing
#include <vector>

#include "CostFunction.h"
#include "Polynomial.h"
template <class T>
class CostFunctionPolynomial : public CostFunction<T> {
	Polynomial<T> polynomial;
	size_t dim;
public:
	CostFunctionPolynomial(Polynomial<T> & inputPolynomial) {
		polynomial = inputPolynomial;
		dim = polynomial.getDim();
	};
	CostFunctionPolynomial(std::vector<T>& c, size_t deg, size_t
		d) {
		dim = d;	
	};
	~CostFunctionPolynomial() {};
	size_t Dimensions() { return dim;}
	T Evaluate(std::vector<T>& x, std::vector<T>& g, size_t) {
		T f = polynomial.EvaluatePolynomial(x);
		polynomial.GradientPolynomial(x, g);
		return f;
	}
	void IterationOutput(std::vector<T>&, std::vector<T>&, size_t) {}
};

template class CostFunctionPolynomial<double>;
template class CostFunctionPolynomial<float>;
