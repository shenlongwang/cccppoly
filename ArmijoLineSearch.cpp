//Author: Alexander G. Schwing
#include <iostream>
#include <numeric>
#include <algorithm>
#include <cmath>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

#include "ArmijoLineSearch.h"

template <class T>
ArmijoLineSearch<T>::ArmijoLineSearch(CostFunction<T>* CF, std::vector<T>* d, T f, std::vector<T>* g) {
	this->CF = CF;
	this->d = d;
	this->f = f;
	this->g = g;
	this->gtd = std::inner_product(g->begin(), g->end(), d->begin(), T(0.0));

	c1 = T(1e-4);
	debug = false;
	LS_interp = 1;
	progTol = T(1e-25);
	MaxPolyDeg = 7;//std::numeric_limits<size_t>::max();
}

template <class T>
ArmijoLineSearch<T>::~ArmijoLineSearch() {

}

template <class T>
T ArmijoLineSearch<T>::GetGTD() {
	return gtd;
}

template <class T>
void ArmijoLineSearch<T>::PolyValHorner(std::vector<T>& coeff, std::vector<T>& xVals, std::vector<T>& yVals) {
	yVals.assign(xVals.size(), coeff[0]);
	for(typename std::vector<T>::iterator coeffIT=coeff.begin()+1,coeffIT_e=coeff.end();coeffIT!=coeffIT_e;++coeffIT) {
		for(typename std::vector<T>::iterator it=yVals.begin(),it_e=yVals.end(),itx=xVals.begin();it!=it_e;++it, ++itx) {
			*it  = *itx**it + *coeffIT;
		}
	}
}

template <class T>
void ArmijoLineSearch<T>::roots(std::vector<T>& coeff, std::vector<std::complex<T> >& r) {
	//strip leading zeros
	size_t lz=0;
	while(lz<coeff.size() && coeff[lz]==T(0.0)) {
		++lz;
	}
	if(lz==coeff.size()) {
		r.clear();
		return;
	}

	//strip trailing zeros
	int tz = int(coeff.size())-1;
	while(tz>=0 && coeff[tz]==T(0.0)) {
		--tz;
	}
	if(tz<int(coeff.size())-1) {
		r.assign(coeff.size()-1-tz, std::complex<T>(T(0.0),T(0.0)));
	} else {
		r.clear();
	}

	assert(tz>=int(lz));
	std::vector<T> tempCoeff(tz-lz+1);
	std::copy(coeff.begin()+lz, coeff.begin()+tz+1, tempCoeff.begin());

	std::vector<T> d(tz-lz);
	std::copy(tempCoeff.begin()+1, tempCoeff.end(), d.begin());
	std::transform(d.begin(), d.end(), d.begin(), std::bind1st(std::multiplies<T>(), T(1.0)/tempCoeff[0]));

	//prevent small leading coefficients from introducing numerical instabilities
	T maxVal = T(0.0);
	for(size_t k=0;k<d.size();++k) {
		maxVal = (fabs(d[k])>maxVal) ? fabs(d[k]) : maxVal;
	}
	while(maxVal>1e30) {
		if(debug) {
			std::cout << "Numerical instability possible: " << maxVal << std::endl;
		}
		std::vector<T>(tempCoeff.begin()+1, tempCoeff.end()).swap(tempCoeff);//using a deque is probably more efficient here

		d.resize(d.size()-1);
		std::copy(tempCoeff.begin()+1, tempCoeff.end(), d.begin());
		std::transform(d.begin(), d.end(), d.begin(), std::bind1st(std::multiplies<T>(), T(1.0)/tempCoeff[0]));

		maxVal = T(0.0);
		for(size_t k=0;k<d.size();++k) {
			maxVal = (fabs(d[k])>maxVal) ? fabs(d[k]) : maxVal;
		}
	}

	//compute roots via companion matrix
	size_t n = tempCoeff.size();
	if(n>1) {
		Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> Comp(Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>::Zero(n-1,n-1));
		for(size_t k=0;k<n-1;++k) {
			Comp(0,k) = -d[k];
			if(k>0) {
				Comp(k,k-1) = T(1.0);
			}
		}
		//Eigen::EigenSolver<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> > es(Comp);
		Eigen::Matrix<std::complex<T>, Eigen::Dynamic,1> eigVals(Comp.eigenvalues());

		size_t rows = eigVals.rows();
		for(size_t k=0;k<rows;++k) {
			r.push_back(eigVals(k));
		}
	}
}

template <class T>
T ArmijoLineSearch<T>::polyinterp(std::vector<T>& tVals, std::vector<T>& fVals, std::vector<T>& gtdVals, T minVal, T maxVal, size_t degree) {
	if(debug) {
		std::cout << "Poly([";
		for(typename std::vector<T>::iterator it=tVals.begin(),it_e=tVals.end();it!=it_e;++it) {
			std::cout << *it << " ";
		}
		std::cout << "]," << std::endl;
		std::cout << "     [";
		for(typename std::vector<T>::iterator it=fVals.begin(),it_e=fVals.end();it!=it_e;++it) {
			std::cout << *it << " ";
		}
		std::cout << "]," << std::endl;
		std::cout << "     [";
		for(typename std::vector<T>::iterator it=gtdVals.begin(),it_e=gtdVals.end();it!=it_e;++it) {
			std::cout << *it << " ";
		}
		std::cout << "], " << minVal << ", " << maxVal << ") = ";// << std::endl;
	}

	size_t order = std::min(2*tVals.size() - 1, degree);

	Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> A(2*tVals.size(),order+1);
	Eigen::Matrix<T, Eigen::Dynamic,1> b(2*tVals.size());

	for(size_t k=0;k<tVals.size();++k) {
		for(size_t m=0;m<order+1;++m) {
			A(k,m) = std::pow(tVals[k], T(order-m));
		}
		b(k) = fVals[k];
	}

	for(size_t k=0;k<tVals.size();++k) {
		for(size_t m=0;m<order;++m) {
			A(k+tVals.size(),m) = T(order-m)*std::pow(tVals[k], T(order-m-1));
		}
		A(k+tVals.size(),order) = T(0.0);
		b(k+tVals.size()) = gtdVals[k];
	}
	/*std::cout << A << std::endl;
	std::cout << b << std::endl;*/

	//another variant: fullPivHouseholderQr()
	//Eigen::Matrix<T, Eigen::Dynamic,1> x(A.colPivHouseholderQr().solve(b));
	Eigen::Matrix<T, Eigen::Dynamic,1> x2(A.fullPivLu().solve(b));
	//Eigen::Matrix<T, Eigen::Dynamic,1> x(A.fullPivHouseholderQr().solve(b));
	Eigen::Matrix<T, Eigen::Dynamic,1> x1;
	if(order==2*tVals.size()-1) {
		x1 = A.partialPivLu().solve(b);
	} else {
		//other possibilities:
		// A.colPivHouseholderQr().solve(b)
		// (A.transpose() * A).ldlt().solve(A.transpose() * b)
		x1 = A.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b);
	}
	//Eigen::Matrix<T, Eigen::Dynamic,1> x(A.ldlt().solve(b));
	//Eigen::Matrix<T, Eigen::Dynamic,1> x3(A.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b));

	//Eigen::Matrix<T, Eigen::Dynamic,1> test(A*x);
	//Eigen::Matrix<T, Eigen::Dynamic,1> test2(A*x-b);
	T n1 = (A*x1-b).norm();
	T n2 = (A*x2-b).norm();
	//T n3 = (A*x3-b).norm();
	//T relative_error = n1 / n2;
	Eigen::Matrix<T, Eigen::Dynamic,1> x(order+1);
	if(n1<n2) {
		x = x1;
	} else {
		x = x2;
	}

	std::vector<T> Coeff(order+1, x(0));
	std::vector<T> dfCoeff(order, T(0.0));
	for(size_t k=0;k<order;++k) {
		dfCoeff[k] = T(order-k)*x(k);
		Coeff[k+1] = x(k+1);
	}

	std::vector<std::complex<T> > rt;
	roots(dfCoeff, rt);

	std::vector<T> cp;
	cp.push_back(minVal);cp.push_back(maxVal);
	for(typename std::vector<T>::iterator it=tVals.begin(),it_e=tVals.end();it!=it_e;++it) {
		if(*it>=minVal && *it<=maxVal) {
			cp.push_back(*it);
		}
	}
	for(typename std::vector<std::complex<T> >::iterator it=rt.begin(),it_e=rt.end();it!=it_e;++it) {
		if(fabs(it->imag())<1e-5) {
			T val = it->real();
			if(val>=minVal && val<=maxVal) {
				cp.push_back(val);
			}
		}
	}
	std::vector<T> cpFunVals(cp.size(), T(0.0));
	PolyValHorner(Coeff, cp, cpFunVals);

	//test critical points
	T fmin = std::numeric_limits<T>::max();
	T minPos = T(0.0);
	for(typename std::vector<T>::iterator it=cpFunVals.begin(),it_e=cpFunVals.end(),cpi=cp.begin();it!=it_e;++it, ++cpi) {
		if(*it<fmin) {
			fmin = *it;
			minPos = *cpi;
		}
	}
	if(debug) {
		std::cout << "(" << minPos << ", " << fmin << ")" << std::endl;
	}
	return minPos;
}

template <class T>
T ArmijoLineSearch<T>::Search(std::vector<T>& x, T t, T* f_out, std::vector<T>& grad_out, size_t iter) {
	std::vector<T> tVals(1, T(0.0));
	std::vector<T> fVals(1, f);
	std::vector<T> gtdVals(1, gtd);

	//T tMax = 100*t;

	// Evaluate the Objective and Gradient at the Initial Step
	std::vector<T> x_new(x);
	for(typename std::vector<T>::iterator it=x_new.begin(),it_e=x_new.end(),it1=d->begin();it!=it_e;++it,++it1) {
		*it += t**it1;
	}
	std::vector<T> g_new;
	T f_new = CF->Evaluate(x_new, g_new, iter);
	T gtd_new = std::inner_product(g_new.begin(), g_new.end(), d->begin(), T(0.0));

	if(debug) {
		std::cout << "t_init: " << t << " --> f_new: " << f_new << ", gtd_new: " << gtd_new << std::endl;
	}

	tVals.push_back(t);
	fVals.push_back(f_new);
	gtdVals.push_back(gtd_new);

	while(f_new > f + c1*t*gtd) {
		T temp = t;

		if(LS_interp==0) {
			if(debug) {
				std::cout << "Fixed Backtracking" << std::endl;
			}
			t = T(0.5)*t;
		} else {
			if(debug) {
				std::cout << "Polynomial Interpolation" << std::endl;
			}
			//t = polyinterp(tVals, fVals, gtdVals, 0, tMax);
			t = polyinterp(tVals, fVals, gtdVals, 0, t, MaxPolyDeg);
		}

		// Adjust if change in t is too small/large
		if(t < temp*T(1e-3)) {
			if(debug) {
				std::cout << "Interpolated Value Too Small, Adjusting from " << t << " to " << temp*T(1e-2) << std::endl;
			}
			//t = temp*T(1e-3);
			t = temp*T(1e-2);
		} /*else if(t > temp*T(5.0)) {
			if(debug) {
				std::cout << "Interpolated Value Too Large, Adjusting" << std::endl;
			}
			t = temp*T(5.0);
		}*/ else if(t > temp*T(0.6)) {
			if(debug) {
				std::cout << "Interpolated Value Too Large, Adjusting from " << t << " to " << temp*T(0.6) << std::endl;
			}
			t = temp*T(0.6);
		}
    
		x_new = x;
		for(typename std::vector<T>::iterator it=x_new.begin(),it_e=x_new.end(),it1=d->begin();it!=it_e;++it,++it1) {
			*it += t**it1;
		}
		f_new = CF->Evaluate(x_new, g_new, iter);
		gtd_new = std::inner_product(g_new.begin(), g_new.end(), d->begin(), T(0.0));

		if(debug) {
			std::cout << "t: " << t << " --> f_new: " << f_new << ", gtd_new: " << gtd_new << std::endl;
		}

		tVals.push_back(t);
		fVals.push_back(f_new);
		gtdVals.push_back(gtd_new);

		// Check whether step size has become too small
		T maxAbsTD = T(0.0);
		for(typename std::vector<T>::iterator it=d->begin(),it_e=d->end();it!=it_e;++it) {
			T buf = fabs(t**it);
			maxAbsTD = (buf>maxAbsTD) ? buf : maxAbsTD;
		}
		if(maxAbsTD <= progTol) {
			if(debug) {
				std::cout << "Backtracking Line Search Failed with " << maxAbsTD << std::endl;
			}
			*f_out = f;
			grad_out = *g;
			return T(0.0);
		}
	}

	*f_out = f_new;
	grad_out = g_new;
	return t;
}

template class ArmijoLineSearch<double>;
template class ArmijoLineSearch<float>;