#include <iostream>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include <string>
#include <iostream>
#include <sstream>
#include "CostFunctionRosenbrock.h"
#include "CostFunctionPolynomial.h"
#include "CostFunctionRegistration.h"
#include "CostFunctionCCCP.h"
#include "CostFunctionCCCPRegistration.h"


#include "Optimizer.h"


size_t testLBFGSRegistration()
{
	double average_energy = 0.0;
	double average_time = 0.0;
	for (size_t i = 1; i <= 1; i++)
	{
		std::stringstream stringinput;//create a stringstream
		stringinput << "./polydata/model" << i << ".txt";
		std::string filename = stringinput.str();
		CostFunctionRegistration<double> CFR(filename);
		Optimizer<double> Opt(100);
		std::vector<double> x_init(CFR.Dimensions(), 160.0);
		// x_init = { 143.4660, 170.9362, 180.3597, 171.1443, 148.8573, 161.5994, 144.8218, 124.0003, 117.1434 };
		// x_init = { 124.1888, 132.1950, 177.5983, 122.2054, 148.0211, 185.3829, 136.0499, 170.3853, 197.4703 };
		clock_t t = clock();
		Opt.Optimize(&CFR, &x_init);
		t = clock() - t;
		std::cout << "Time elapse: " << ((float)t) / CLOCKS_PER_SEC << " seconds." << std::endl;
		std::cout << "Solution: " << std::endl;
		for (std::vector<double>::iterator it = x_init.begin(), it_e = x_init.end(); it != it_e; ++it) {
			std::cout << *it << std::endl;
		}
		std::vector<double> g(CFR.Dimensions(), 0.5);
		std::cout << "Energy: " << std::endl;
		size_t k = 0;
		std::cout << CFR.Evaluate(x_init, g, k);


		std::stringstream stringoutput;//create a stringstream
		stringoutput << "./polydata/outputRegistration" << i << ".txt";//add number to the stream
		filename = stringoutput.str();
		std::ofstream outputFile(filename, std::ios_base::out);
		if (outputFile.is_open())
		{
			outputFile << CFR.Evaluate(x_init, g, t) << std::endl;
			outputFile << ((float)t) / CLOCKS_PER_SEC << std::endl;
			for (std::vector<double>::iterator it = x_init.begin(), it_e = x_init.end(); it != it_e; ++it) {
				outputFile << *it << " ";
			}
		}
		average_energy += CFR.Evaluate(x_init, g, k);
		average_time += ((float)t) / CLOCKS_PER_SEC;
	}
	std::cout << "Energy: " << average_energy / 100.0 << std::endl;
	std::cout << "Time: " << average_time / 100.0 << std::endl;

	return 0;
}

size_t testLBFGSRosenbrock()
{
	CostFunctionRosenbrock<double> CFR;
	Optimizer<double> Opt(100);
	std::vector<double> x_init(CFR.Dimensions(), 0.5);
	Opt.Optimize(&CFR, &x_init);
	std::cout << "Solution: " << std::endl;
	for (std::vector<double>::iterator it = x_init.begin(), it_e = x_init.end(); it != it_e; ++it) {
		std::cout << *it << std::endl;
	}
	return 0;
}

size_t testLBFGS()
{
	// Polynomial<double> rosenbrock("rosenbrock.txt");
	// CostFunctionPolynomial<double> CFR(rosenbrock);
	// Polynomial<double> fvex("fvex16.txt");
	// Polynomial<double> fcave("fcave16.txt");
	Polynomial<double> fvex("fvex16.txt");
	Polynomial<double> fcave("fcave16.txt");
	Polynomial<double> f = fvex;
	f.PlusPolynomial(fcave);
	CostFunctionPolynomial<double> CFR(f);

	Optimizer<double> Opt(2000);
	std::vector<double> x_init(CFR.Dimensions(), 0.5);
	for (size_t i = 0; i < x_init.size(); i++)
		x_init[i] = ((double)rand() / (RAND_MAX + 1));
	Opt.Optimize(&CFR, &x_init);
	std::cout << "Solution: " << std::endl;
	for (std::vector<double>::iterator it = x_init.begin(), it_e = x_init.end(); it != it_e; ++it) {
		std::cout << *it << std::endl;
	}
	std::ofstream outputFile("outputLBFGS.txt", std::ios_base::out);
	if (outputFile.is_open())
	{
		for (std::vector<double>::iterator it = x_init.begin(), it_e = x_init.end(); it != it_e; ++it) {
			outputFile << *it << " ";
		}
	}
	return 0;
}

size_t testCCCPRegistration()
{
	size_t maxIter = 500;
	double epsilon = 1e-5;
	double fValueOld = 0.0;
	double fValueNew = 0.0;
	size_t k = 0;
	
	for (size_t i = 1; i <= 100; i++)
	{
		std::stringstream stringinput;//create a stringstream
		
		// /* --------------------------------3x3 Mesh------------------------------ */
		 stringinput << "./polydata/model" << i << ".txt";
		 std::string filename = stringinput.str();
		 CostFunctionCCCPRegistration<double> CFR(filename);
		 CostFunctionRegistration<double> forigin(filename);
		
		// /* --------------------------------9x9 Mesh------------------------------ */
		/*stringinput << "./polydata/cardboard" << i << ".txt";
		std::string filename = stringinput.str();
		CostFunctionCCCPRegistration<double> CFR(filename, "./polydata/edge_9x9.txt");
		CostFunctionRegistration<double> forigin(filename, "./polydata/edge_9x9.txt");*/

		std::vector<double> x_init(CFR.Dimensions(), 156.0);
		// x_init = { 143.4660, 170.9362, 180.3597, 171.1443, 148.8573, 161.5994, 144.8218, 124.0003, 117.1434 };
		// x_init = { 120.0,  150.0,  180.0,  120.0,  150.0,  180.0,  120.0,  150.0,  180.0 };
		std::vector<double> energy(maxIter, 0.0);
		std::vector<double> time(maxIter, 0.0);
		std::vector<double> g(CFR.Dimensions(), 0.0);

		clock_t t = clock();
		size_t numIter = 0;
		fValueNew = forigin.Evaluate(x_init, g, k);
		for (size_t iter = 0; iter < maxIter; iter++)
		{
			fValueOld = fValueNew;
			Optimizer<double> Opt(100);
			CFR.SetGradient(x_init);
			Opt.Optimize(&CFR, &x_init);
			fValueNew = forigin.Evaluate(x_init, g, k);
			std::cout << "Iter " << iter << ", Current Energy: " << fValueNew << std::endl;
			if (fabs(fValueNew - fValueOld) < epsilon)
			{
				numIter = iter;
				break;
			}
			energy[iter] = fValueOld;
			time[iter] = (float)(clock() - t) / CLOCKS_PER_SEC;
		}
		if (numIter == 0)
			numIter = maxIter;
		std::stringstream stringoutput;//create a stringstream
		stringoutput << "./polydata/150nowait/outputCCCP" << i << ".txt";//add number to the stream
		filename = stringoutput.str();
		std::ofstream outputFile(filename, std::ios_base::out);
		if (outputFile.is_open())
		{
			for (std::vector<double>::iterator it = x_init.begin(), it_e = x_init.end(); it != it_e; ++it) {
				outputFile << *it << " ";
			}
			outputFile << std::endl;
			for (size_t i = 0; i < numIter; i++)
				outputFile << energy[i] << " ";
			outputFile << std::endl;
			for (size_t i = 0; i < numIter; i++)
				outputFile << time[i] << " ";
			outputFile << std::endl;
		}
	}
	return 0;
}

size_t testCCCP()
{
	size_t maxIter = 500;
	double epsilon = 1e-5;
	double fValueOld = 0.0;
	double fValueNew = 0.0;
	// Polynomial<double> fvex("rosenbrockvex.txt");
	// Polynomial<double> fcave("rosenbrockcave.txt");
	// Polynomial<double> fvex("simpleconvex.txt");
	// Polynomial<double> fcave("simpleconcave.txt");
	Polynomial<double> fvex("fvex16.txt");
	Polynomial<double> fcave("fcave16.txt");
	// Polynomial<double> fvex("fvex8.txt");
	// Polynomial<double> fcave("fcave8.txt");
	// Polynomial<double> fvex("simplevex1d.txt");
	// Polynomial<double> fcave("simplecave1d.txt");
	Polynomial<double> f = fvex;
	f.PlusPolynomial(fcave);
	fvex.DisplayPolynomial();
	fcave.DisplayPolynomial();

	std::vector<double> x(fvex.getDim(), 0.5);
	for (size_t i = 0; i < x.size(); i++)
		x[i] = ((double)rand() / (RAND_MAX + 1));
	std::vector<double> gcave(fvex.getDim(), 0.0);
	fValueNew = f.EvaluatePolynomial(x);
	std::vector<double> energy(maxIter, 0.0);
	std::vector<double> time(maxIter, 0.0);
	clock_t t = clock();
	size_t numIter = 0;
	for (size_t i = 0; i < maxIter; i++)
	{
		fValueOld = fValueNew;
		fcave.GradientPolynomial(x, gcave);
		CostFunctionCCCP<double> CFR(fvex, gcave);
		Optimizer<double> Opt(100);
		Opt.Optimize(&CFR, &x);
		//std::cout << "Solution: " << std::endl;
		//for (std::vector<double>::iterator it = x.begin(), it_e = x.end(); it != it_e; ++it) {
		//	std::cout << *it << std::endl;
		//}
		fValueNew = f.EvaluatePolynomial(x);
		std::cout <<"Iter "<<i<< ", Current Energy: " << fValueNew <<std::endl;
		if (fabs(fValueNew - fValueOld) < epsilon)
		{
			numIter = i;
			break;
		}
		energy[i] = fValueOld;
		time[i] = (float)(clock() - t) / CLOCKS_PER_SEC;
	}
	if (numIter == 0)
		numIter = maxIter;
	t = clock() - t;
	std::cout << "Time elapse: " << ((float)t) / CLOCKS_PER_SEC << " seconds." << std::endl;

	std::ofstream outputFile("output.txt", std::ios_base::out);
	if (outputFile.is_open())
	{
		for (std::vector<double>::iterator it = x.begin(), it_e = x.end(); it != it_e; ++it) {
			outputFile << *it << " ";
		}
		outputFile << std::endl;
		for (size_t i = 0; i < numIter; i++)
			outputFile << energy[i]<< " ";
		outputFile << std::endl;
		for (size_t i = 0; i < numIter; i++)
			outputFile << time[i] << " ";
		outputFile << std::endl;
	}


	return 0;
}

size_t testPolynomial()
{
	// test monomial class
	size_t dim = 3;
	size_t deg = 6;

	std::vector<size_t> variables{ 1, 2 };
	std::vector<size_t> degrees{ 2, 4 };
	double coeff = 2.1;
	Monomial<double> testMonomial(variables, degrees, coeff, dim);

	std::cout << "Test monomial is ";
	testMonomial.DisplayMonomial(); std::cout << std::endl;
	std::vector<double> x{ 1.0, 3.0, 2.0 };
	double fValue = testMonomial.EvaluateMonomial(x);
	std::cout << "Monomial function value at point (" << x[0] << ", " << x[1] << ", " << x[2] << ") is ";
	std::cout << fValue << std::endl;
	// std::cout << 2.1*3.0*3.0*16.0 << std::endl;
	std::cout << "Deriviate wrt to x_" << testMonomial.GetVariable(0) << " is ";
	Monomial<double> gradMonomial = testMonomial.GradientMonomial(0);
	gradMonomial.DisplayMonomial(); std::cout << std::endl;

	std::vector<size_t> variables2{ 0, 2 };
	std::vector<size_t> degrees2{ 3, 1 };
	double coeff2 = -1.2;
	Monomial<double> testMonomial2(variables2, degrees2, coeff2, dim);

	std::vector< Monomial<double>> monomials = { testMonomial, testMonomial2 };
	Polynomial<double> testPolynomial(monomials, deg, dim);
	testPolynomial.DisplayPolynomial(); std::cout << std::endl;
	fValue = testPolynomial.EvaluatePolynomial(x);
	std::cout << "Polynomial function value at point (" << x[0] << ", " << x[1] << ", " << x[2] << ") is ";
	std::cout << fValue << std::endl;

	std::vector<double> g;
	testPolynomial.GradientPolynomial(x, g);

	std::cout << "Polynomial gradient is (";
	for (size_t i = 0; i < g.size(); i++)
		std::cout << g[i] << " ";
	std::cout << ")" << std::endl;

	//Polynomial<double> testPolynomial2("testpolynomial.txt");
	//testPolynomial2.DisplayPolynomial(); std::cout << std::endl;
	//fValue = testPolynomial2.EvaluatePolynomial(x);
	//std::cout << "Polynomial function value at point (" << x[0] << ", " << x[1] << ", " << x[2] << ") is ";
	//std::cout << fValue << std::endl;
	//testPolynomial2.GradientPolynomial(x, g);
	//std::cout << "Polynomial gradient is (";
	//for (size_t i = 0; i < g.size(); i++)
	//	std::cout << g[i] << " ";
	//std::cout << ")" << std::endl;

	Polynomial<double> rosenbrock("rosenbrock.txt");
	rosenbrock.DisplayPolynomial(); 
	x.resize(2);
	g.resize(2);
	fValue = rosenbrock.EvaluatePolynomial(x);
	std::cout << "Polynomial function value at point (" << x[0] << ", " << x[1] << ") is ";
	std::cout << fValue << std::endl;
	rosenbrock.GradientPolynomial(x, g);
	std::cout << "Polynomial gradient is (";
	for (size_t i = 0; i < g.size(); i++)
		std::cout << g[i] << " ";
	std::cout << ")" << std::endl;

	return 0;
}

int main() {
	
	testCCCP();
	// testLBFGSRosenbrock();
	// testLBFGS();
	// testPolynomial();
	// testLBFGSRegistration();
	// testCCCPRegistration();
	std::cout << "Press anykey to exit.";
	std::cin.ignore();
	return 0;
}
