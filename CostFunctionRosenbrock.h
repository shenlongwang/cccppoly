//Author: Alexander G. Schwing
#include <vector>

#include "CostFunction.h"

template <class T>
class CostFunctionRosenbrock : public CostFunction<T> {
	size_t dim;
public:
	CostFunctionRosenbrock() {
		dim = 2;	
	};
	~CostFunctionRosenbrock() {};
	size_t Dimensions() { return dim;}
	T Evaluate(std::vector<T>& x, std::vector<T>& g, size_t) {
		T f = T(0.0);
		for(size_t k=1;k<dim;++k) {
			f += 100*(x[k]-x[k-1]*x[k-1])*(x[k]-x[k-1]*x[k-1]) + (1-x[k-1])*(1-x[k-1]);
		}

		g.assign(dim, T(0.0));
		for(size_t k=1;k<dim;++k) {
			g[k-1] = -400*x[k-1]*(x[k]-x[k-1]*x[k-1]) - 2*(1-x[k-1]);
		}
		for(size_t k=1;k<dim;++k) {
			g[k] = g[k] + 200*(x[k]-x[k-1]*x[k-1]);
		}

		return f;
	}
	void IterationOutput(std::vector<T>&, std::vector<T>&, size_t) {}
};

template class CostFunctionRosenbrock<double>;
template class CostFunctionRosenbrock<float>;