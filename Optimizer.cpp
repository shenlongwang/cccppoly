//Author: Alexander G. Schwing
#include <iostream>
#include <algorithm>
#include <numeric>
#include <functional>
#include <limits>
#include <cmath>

#include <assert.h>

#include "Optimizer.h"
#include "WolfeLineSearch.h"
#include "ArmijoLineSearch.h"
//#include "ConstantStep.h"

template <class T>
Optimizer<T>::Optimizer(size_t MaxIterations) {
	LS_init = 2;
	DDmethod = 1;
	cgUpdate = 1;
	debug = false;
	keepBest = true;
	this->MaxIterations = MaxIterations;
}

template <class T>
Optimizer<T>::~Optimizer() {

}

template <class T>
T Optimizer<T>::Optimize(CostFunction<T>* CF, std::vector<T>* x_init) {
	std::vector<T> grad(CF->Dimensions(), 0.0);
	std::vector<T> x(CF->Dimensions(), 0.0);
	if(x_init!=NULL) {
		assert(x_init->size()==CF->Dimensions());
		std::copy(x_init->begin(), x_init->end(), x.begin());
	}
	T f = CF->Evaluate(x, grad, 0);
	CF->IterationOutput(x, grad, size_t(-1));

	std::cout << "f_start: " << f << std::endl;

	std::vector<T> x_best(x);
	T f_best = std::numeric_limits<T>::max();
	
	T gtd_old = T(0.0);
	T f_old = T(0.0);

	T grad_max = -std::numeric_limits<T>::max();
	T sum_abs_g = 0.0;
	for(typename std::vector<T>::iterator git=grad.begin(),git_e=grad.end();git!=git_e;++git) {
		T val = fabs(*git);
		grad_max = (val>grad_max) ? val : grad_max;
		sum_abs_g += val;
	}

	T improvement = T(0.0);
	std::vector<T> g_old(grad.size(), T(0.0));
	std::vector<T> d(grad.size(), T(0.0));

	for(size_t it1=0;it1<MaxIterations;++it1) {
		//descent direction
		if(DDmethod==0) {//steepest descent
			d = grad;
			std::transform(d.begin(), d.end(), d.begin(), std::negate<T>());
		} else if(DDmethod==1) {//non-linear conjugate gradient
			if(it1==0) {
				d = grad;
				std::transform(d.begin(), d.end(), d.begin(), std::negate<T>());
			} else if(it1>0) {
				T gotgo = std::inner_product(g_old.begin(), g_old.end(), g_old.begin(), T(0.0));
				T beta = T(0.0);
				if(cgUpdate==0) {//Fletcher-Reeves
					beta = std::inner_product(grad.begin(), grad.end(), grad.begin(), T(0.0))/gotgo;
				} else if(cgUpdate==1) {//Polak-Ribiere
					for(typename std::vector<T>::iterator git=grad.begin(),git_e=grad.end(),goit=g_old.begin();git!=git_e;++git, ++goit) {
						beta += *git*(*git-*goit);
					}
					beta /= gotgo;
				} else if(cgUpdate==2) {//Hestenes-Stiefel
					T temp1 = T(0.0);
					T temp2 = T(0.0);
					for(typename std::vector<T>::iterator git=grad.begin(),git_e=grad.end(),goit=g_old.begin(),dit=d.begin();git!=git_e;++git, ++goit, ++dit) {
						temp1 += *git*(*git-*goit);
						temp2 += *dit*(*git-*goit);
					}
					beta = temp1/temp2;
				} else {//Gilbert-Nocedal
					T beta_FR = std::inner_product(grad.begin(), grad.end(), grad.begin(), T(0.0))/gotgo;
					T beta_PR = T(0.0);
					for(typename std::vector<T>::iterator git=grad.begin(),git_e=grad.end(),goit=g_old.begin();git!=git_e;++git, ++goit) {
						beta_PR += *git*(*git-*goit);
					}
					beta_PR /= gotgo;
					if(fabs(beta_PR)<=beta_FR) {
						beta = beta_PR;
					} else if(beta_PR>beta_FR) {
						beta = beta_FR;
					} else if(beta_PR<-beta_FR) {
						beta = -beta_FR;
					} else {
						assert(false);
					}
				}
				std::transform(d.begin(), d.end(), d.begin(), std::bind1st(std::multiplies<T>(), beta));
				std::transform(d.begin(), d.end(), grad.begin(), d.begin(), std::minus<T>());

				if(std::inner_product(grad.begin(), grad.end(), d.begin(), T(0.0)) > -T(1e-25)) {
					if(debug) {
						std::cout << "Restarting CG" << std::endl;
					}
					beta = T(0.0);
					d = grad;
					std::transform(d.begin(), d.end(), d.begin(), std::negate<T>());
				}
			}
			g_old = grad;
		}

		if(grad_max<=1e-5) {
			std::cout << "Optimality condition below optTol." << std::endl;
			break;
		}

		WolfeLineSearch<T> LS(CF, &d, f, &grad);
		//ArmijoLineSearch<T> LS(CF, &d, f, &grad);
		//ConstantStep<T> LS(CF, &d, f, &grad);
		T gtd = LS.GetGTD();

		if(debug) {
			std::cout << "  gtd = " << gtd << std::endl;
			/*std::cout << "  f   = " << f << std::endl;
			std::cout << "  f_o = " << f_old << std::endl;*/
		}

		//step length
		T t_init = 0.0;
		if(it1==0) {
			t_init = std::min(T(1.0), T(1.0)/sum_abs_g);
		} else {
			if(LS_init==0) {
				t_init = T(1.0);
				//t_init = T(1e-9);
			} else if(LS_init==1) {
				t_init = t_init*std::min(T(2.0), gtd_old/gtd);
			} else if(LS_init==2) {
				//std::cout << "   here: " << T(2.0)*f/gtd - T(2.0)*f_old/gtd << std::endl;
				t_init = std::min(T(1.0), (T(2.0)*improvement/gtd));
			}
		}
		f_old = f;
		gtd_old = gtd;

		//std::cout << "  t_init = " << t_init << std::endl;

		T step_size = LS.Search(x, t_init, &f, grad, it1);

		/*std::cout << "  grad = [";
		for(typename std::vector<T>::iterator it=grad.begin(),it_e=grad.end();it!=it_e;++it) {
			std::cout << *it << " ";
		}
		std::cout << "]" << std::endl;*/

		T maxStepLength = 0.0;
		grad_max = -std::numeric_limits<T>::max();
		sum_abs_g = T(0.0);
		for(typename std::vector<T>::iterator it=x.begin(),it_e=x.end(),itd=d.begin(),git=grad.begin();it!=it_e;++it,++itd,++git) {
			T val = step_size**itd;
			T val2 = fabs(*git);
			*it += val;
			maxStepLength = (fabs(val)>maxStepLength) ? fabs(val) : maxStepLength;
			grad_max = (val2>grad_max) ? val2 : grad_max;
			sum_abs_g += val2;
		}

		std::cout << "I: " << it1 << " " << step_size << " " << ((keepBest) ? f_best : f_old) << " " << f << " " << grad_max << " ";
		CF->IterationOutput(x, d, it1);
		std::cout << std::endl;

		if(keepBest && f<=f_best) {
			x_best = x;
			f_best = f;
		}

		//std::cout << "File: " << x[0] << " " << d[0] << std::endl;

		improvement = f - f_old;


		//update f and gradient if using iteration dependent cost function
		//f = CF->Evaluate(x, grad, it1+1);
		if(debug) {
			//std::cout << "Datachange: " << f << std::endl;
			std::cout << "Datachange: off" << std::endl;
		}


		//check termination conditions
		if(maxStepLength<1e-20) {
			std::cout << "Steplength below progTol." << std::endl;
			break;
		}

		if(fabs(improvement)<1e-20) {
			std::cout << "Function value changing by less than progTol." << std::endl;
			break;
		}

		if(it1==MaxIterations-1) {
			std::cout << "Maximum number of iterations reached." << std::endl;
			break;
		}
	}
	if(x_init!=NULL) {
		if(keepBest) {
			*x_init = x_best;
		} else {
			*x_init = x;
		}
	}
	return f;
}

template class Optimizer<double>;
template class Optimizer<float>;