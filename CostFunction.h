//Author: Alexander G. Schwing
#ifndef COSTFUNCTION__H__
#define COSTFUNCTION__H__

#include <vector>
#include <cstdint>
#include <stdint.h>
#include <stdio.h>

template <class T>
class CostFunction {
public:
	CostFunction() {};
	virtual ~CostFunction() {};
	virtual size_t Dimensions() = 0;
	virtual T Evaluate(std::vector<T>&, std::vector<T>&, size_t) = 0;
	virtual void IterationOutput(std::vector<T>&, std::vector<T>&, size_t) = 0;
};

#endif