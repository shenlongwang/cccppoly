//Author: Alexander G. Schwing
#include <iostream>
#include <numeric>
#include <algorithm>
#include <cmath>

#include "WolfeLineSearch.h"

template <class T>
WolfeLineSearch<T>::WolfeLineSearch(CostFunction<T>* CF, std::vector<T>* d, T f, std::vector<T>* g) {
	this->CF = CF;
	this->d = d;
	this->f = f;
	this->g = g;
	this->gtd = std::inner_product(g->begin(), g->end(), d->begin(), T(0.0));

	maxLS = 25;
	c1 = T(1e-4);
	c2 = T(0.9);
	LS_interp = 2;
	debug = false;
	progTol = T(1e-25);
}

template <class T>
WolfeLineSearch<T>::~WolfeLineSearch() {

}

template <class T>
T WolfeLineSearch<T>::GetGTD() {
	return gtd;
}

template <class T>
T WolfeLineSearch<T>::polyinterp(T t1, T f1, T gtd1, T t2, T f2, T gtd2, T* minStep, T* maxStep) {
	T xmin = std::min(t1,t2);
	T xmax = std::max(t1,t2);

	//Compute Bounds of Interpolation Area
	T xminBound = xmin;
	T xmaxBound = xmax;
	if(minStep!=NULL) {
		xminBound = *minStep;
	}
	if(maxStep!=NULL) {
		xmaxBound = *maxStep;
	}

	if(debug) {
		std::cout << "Poly(" << t1 << ", " << f1 << ", " << gtd1 << ", " << t2 << ", " << f2 << ", " << gtd2 << ", " << xminBound << ", " << xmaxBound << ") = ";
	}

	//   - cubic interpolation of 2 points  w/ function and derivative values for both
    // Solution in this case (where x2 is the farthest point):
    //    d1 = g1 + g2 - 3*(f1-f2)/(x1-x2);
    //    d2 = sqrt(d1^2 - g1*g2);
    //    minPos = x2 - (x2 - x1)*((g2 + d2 - d1)/(g2 - g1 + 2*d2));
    //    t_new = min(max(minPos,x1),x2);
	T d1 = T(0.0);
	if(t1<t2) {
		d1 = gtd1 + gtd2 - 3*(f1-f2)/(t1-t2);
	} else {
		d1 = gtd1 + gtd2 - 3*(f2-f1)/(t2-t1);
	}
	T d2 = d1*d1 - gtd1*gtd2;
	T t = T(0.0);
	if(d2>=0) {
		d2 = sqrt(d2);
		if(t1<t2) {
			t = t2 - (t2-t1)*(gtd2 + d2 - d1)/(gtd2 - gtd1 + 2*d2);
		} else {
			t = t1 - (t1-t2)*(gtd1 + d2 - d1)/(gtd1 - gtd2 + 2*d2);
		}
		t = std::min(std::max(t,xminBound), xmaxBound);
	} else {
		t = (xminBound+xmaxBound)/T(2.0);
	}
	if(debug) {
		std::cout << t << std::endl;
	}
    return t;
}

template <class T>
T WolfeLineSearch<T>::Search(std::vector<T>& x, T t, T* f_out, std::vector<T>& grad_out, size_t iter) {
	if(gtd > -progTol) {
		std::cout << "Directional Derivative below progTol." << std::endl;
		return 0;
	}

	// Evaluate the Objective and Gradient at the Initial Step
	std::vector<T> x_new(x);
	for(typename std::vector<T>::iterator it=x_new.begin(),it_e=x_new.end(),it1=d->begin();it!=it_e;++it,++it1) {
		*it += t**it1;
	}
	std::vector<T> g_new;
	T f_new = CF->Evaluate(x_new, g_new, iter);
	T gtd_new = std::inner_product(g_new.begin(), g_new.end(), d->begin(), T(0.0));

	if(debug) {
		std::cout << "t_init: " << t << " --> f_new: " << f_new << ", gtd_new: " << gtd_new << std::endl;
	}

	//std::cout << "  gtd_new: " << gtd_new << std::endl;

	//Bracket an Interval containing a point satisfying the Wolfe criteria
	size_t LSiter = 0;
	T t_prev = T(0.0);
	T f_prev = f;
	std::vector<T> g_prev(*g);
	T gtd_prev = gtd;
	T nrmD = T(0.0);
	for(typename std::vector<T>::iterator it=d->begin(),it_e=d->end();it!=it_e;++it) {
		nrmD = (fabs(*it)>nrmD)? fabs(*it) : nrmD;	
	}
	int done = 0;

	std::vector<T> bracket;
	std::vector<T> bracketFval;
	std::vector<std::vector<T> > bracketGval;
	while(LSiter < maxLS) {
		//Bracketing Phase
		if(f_new > f + c1*t*gtd || (LSiter > 0 && f_new >= f_prev) ) {
			bracket.push_back(t_prev);bracket.push_back(t);
			bracketFval.push_back(f_prev);bracketFval.push_back(f_new);
			bracketGval.push_back(g_prev);bracketGval.push_back(g_new);
			if(debug) {
				std::cout << "Break for reason 1." << std::endl;
			}
			break;
		} else if(fabs(gtd_new) <= -c2*gtd) {
			bracket.push_back(t);
			bracketFval.push_back(f_new);
			bracketGval.push_back(g_new);
			done = 1;
			if(debug) {
				std::cout << "Break for reason 2." << std::endl;
			}
			break;
		} else if(gtd_new >= 0) {
			bracket.push_back(t_prev);bracket.push_back(t);
			bracketFval.push_back(f_prev);bracketFval.push_back(f_new);
			bracketGval.push_back(g_prev);bracketGval.push_back(g_new);
			if(debug) {
				std::cout << "Break for reason 3." << std::endl;
			}
			break;
		}

		T temp = t_prev;
		t_prev = t;
		T minStep = t + T(0.01)*(t-temp);
		T maxStep = t*T(10.0);

		if(LS_interp <= 1) {
			if(debug) {
				std::cout << "Extending Bracket." << std::endl;
			}
			t = maxStep;
		} else if(LS_interp == 2) {
			if(debug) {
				std::cout << "Cubic Extrapolation." << std::endl;
			}
			/*std::cout << "x = [";
			for(typename std::vector<T>::iterator it=x.begin(),it_e=x.end();it!=it_e;++it) {
				std::cout << *it << " ";
			}
			std::cout << "]" << std::endl;*/
			t = polyinterp(temp, f_prev, gtd_prev, t, f_new, gtd_new, &minStep, &maxStep);
		}
    
		f_prev = f_new;
		g_prev = g_new;
		gtd_prev = gtd_new;

		x_new = x;
		for(typename std::vector<T>::iterator it=x_new.begin(),it_e=x_new.end(),it1=d->begin();it!=it_e;++it,++it1) {
			*it += t**it1;
		}
		f_new = CF->Evaluate(x_new, g_new, iter);
		gtd_new = std::inner_product(g_new.begin(), g_new.end(), d->begin(), T(0.0));
		++LSiter;

		if(debug) {
			std::cout << "t: " << t << " --> f_new: " << f_new << ", gtd_new: " << gtd_new << std::endl;
		}
	}

	if(LSiter == maxLS) {
		bracket.push_back(0);bracket.push_back(t);
		bracketFval.push_back(f);bracketFval.push_back(f_new);
		bracketGval.push_back(*g);bracketGval.push_back(g_new);
	}

	// Zoom Phase
	// We now either have a point satisfying the criteria, or a bracket surrounding a point satisfying the criteria
	// Refine the bracket until we find a point satisfying the criteria
	int insufProgress = 0;
	while(!done && LSiter < maxLS) {

		//Find High and Low Points in bracket
		typename std::vector<T>::iterator it_tmp = std::min_element(bracketFval.begin(), bracketFval.end());
		T f_LO = *it_tmp;
		size_t LOpos = it_tmp-bracketFval.begin();
		size_t HIpos = 1 - LOpos;

		// Compute new trial value
		if(LS_interp <= 1) {
			if(debug) {
				std::cout << "Bisecting." << std::endl;
			}
			t = std::accumulate(bracket.begin(), bracket.end(), T(0.0))/bracket.size();
		} else if(LS_interp == 2) {
			if(debug) {
				std::cout << "Cubic interpolation." << std::endl;
			}
			/*std::cout << "x = [";
			for(typename std::vector<T>::iterator it=x.begin(),it_e=x.end();it!=it_e;++it) {
				std::cout << *it << " ";
			}
			std::cout << "]" << std::endl;*/
			t = polyinterp(bracket[0], bracketFval[0], std::inner_product(bracketGval[0].begin(), bracketGval[0].end(), d->begin(), T(0.0)),
            bracket[1], bracketFval[1], std::inner_product(bracketGval[1].begin(), bracketGval[1].end(), d->begin(), T(0.0)), NULL, NULL);
		}


		//Test that we are making sufficient progress
		T maxBracket = *std::max_element(bracket.begin(), bracket.end());
		T minBracket = *std::min_element(bracket.begin(), bracket.end());
		if(std::min(maxBracket-t,t-minBracket)/(maxBracket-minBracket) < 0.1) {
			if(debug) {
				std::cout << "Interpolation close to boundary" << std::endl;
			}
			if(insufProgress || t>=maxBracket || t <= minBracket) {
				if(debug) {
					std::cout << "Evaluating at 0.1 away from boundary" << std::endl;
				}
				if(fabs(t-maxBracket) < fabs(t-minBracket)) {
					t = maxBracket-T(0.1)*(maxBracket-minBracket);
				} else {
					t = minBracket+T(0.1)*(maxBracket-minBracket);
				}
				insufProgress = 0;
			} else {
				if(debug) {
					std::cout << "Insufficient Progress." << std::endl;
				}
				insufProgress = 1;
			}
		} else {
			insufProgress = 0;
		}

		//Evaluate new point
		x_new = x;
		for(typename std::vector<T>::iterator it=x_new.begin(),it_e=x_new.end(),it1=d->begin();it!=it_e;++it,++it1) {
			*it += t**it1;
		}
		f_new = CF->Evaluate(x_new, g_new, iter);
		gtd_new = std::inner_product(g_new.begin(), g_new.end(), d->begin(), T(0.0));
		++LSiter;

		if(debug) {
			std::cout << "t: " << t << " --> f_new: " << f_new << ", gtd_new: " << gtd_new << std::endl;
		}

		bool armijo = (f_new < f + c1*t*gtd);
		if(!armijo || f_new >= f_LO) {
			//Armijo condition not satisfied or not lower than lowest point
			bracket[HIpos] = t;
			bracketFval[HIpos] = f_new;
			bracketGval[HIpos] = g_new;
		} else {
			if(fabs(gtd_new) <= - c2*gtd) {
				//Wolfe conditions satisfied
				done = 1;
			} else if(gtd_new*(bracket[HIpos]-bracket[LOpos]) >= 0) {
				// Old HI becomes new LO (wrong)
				bracket[HIpos] = bracket[LOpos];
				bracketFval[HIpos] = bracketFval[LOpos];
				bracketGval[HIpos] = bracketGval[LOpos];
			}
			// New point becomes new LO
			bracket[LOpos] = t;
			bracketFval[LOpos] = f_new;
			bracketGval[LOpos] = g_new;
		}

		if(!done && fabs(bracket[0]-bracket[1])*nrmD < progTol) {
			if(debug) {
				std::cout << "Line-search bracket has been reduced below progTol" << std::endl;
			}
			break;
		}

	}

	if(LSiter == maxLS) {
		if(debug) {
			std::cout << "Line Search Exceeded Maximum Line Search Iterations" << std::endl;
		}
	}

	typename std::vector<T>::iterator it_tmp = std::min_element(bracketFval.begin(), bracketFval.end());
	*f_out = *it_tmp;
	size_t LOpos = it_tmp-bracketFval.begin();
	t = bracket[LOpos];
	grad_out = bracketGval[LOpos];
	return t;
}

template class WolfeLineSearch<double>;
template class WolfeLineSearch<float>;