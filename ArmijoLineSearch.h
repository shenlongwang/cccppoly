//Author: Alexander G. Schwing
#include <complex>
#include <vector>
#include "CostFunction.h"

template <class T>
class ArmijoLineSearch {
	T f;
	T gtd;
	std::vector<T>* d;
	std::vector<T>* g;
	CostFunction<T>* CF;

	T c1;
	int LS_interp;
	bool debug;
	T progTol;
	size_t MaxPolyDeg;
public:
	ArmijoLineSearch(CostFunction<T>* CF, std::vector<T>* d, T f, std::vector<T>* g);
	~ArmijoLineSearch();

	T Search(std::vector<T>& x, T t, T* f_out, std::vector<T>& grad_out, size_t iter);
	T GetGTD();

	T polyinterp(std::vector<T>& tVals, std::vector<T>& fVals, std::vector<T>& gtdVals, T minVal, T maxVal, size_t degree);
	void roots(std::vector<T>& coeff, std::vector<std::complex<T> >& r);
	void PolyValHorner(std::vector<T>& coeff, std::vector<T>& xVals, std::vector<T>& yVals);
};