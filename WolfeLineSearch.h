//Author: Alexander G. Schwing
#include <vector>
#include "CostFunction.h"

template <class T>
class WolfeLineSearch {
	T f;
	T gtd;
	std::vector<T>* d;
	std::vector<T>* g;
	CostFunction<T>* CF;

	size_t maxLS;
	T c1;
	T c2;
	T progTol;
	int LS_interp;
	bool debug;

	T polyinterp(T t1, T f1, T gtd1, T t2, T f2, T gtd2, T* minStep, T* maxStep);
public:
	WolfeLineSearch(CostFunction<T>* CF, std::vector<T>* d, T f, std::vector<T>* g);
	~WolfeLineSearch();

	T Search(std::vector<T>& x, T t, T* f_out, std::vector<T>& grad_out, size_t iter);
	T GetGTD();
};