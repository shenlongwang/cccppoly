// Author: Shenlong Wang
#ifndef POLYNOMIAL__H__ 
#define POLYNOMIAL__H__ 

#include <vector>
#include <cstdint>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <stdint.h>
#include <stdio.h>
#include "CostFunction.h"

#define MAX_MONOMIAL 10  // highest order of monomials (number of different variables)

template <class T>
class Monomial
{
public:
	Monomial(std::vector<size_t> inputVariables, std::vector<size_t> inputDegrees, T inputCoeff, size_t inputDim)
	{
		variables = inputVariables; 
		degrees = inputDegrees;  
		coeff = inputCoeff; 
		dim = inputDim;
		numVariable = inputVariables.size();// number of variables
		deg = -1; // degree of the monomial, caculated when used. 
	};
	~Monomial(){};
	
	Monomial<T> GradientMonomial(size_t index)
	{
		std::vector<size_t> gradVariables = variables;
		std::vector<size_t> gradDegrees = degrees;
		T gradCoeff = coeff;
		if (degrees[index] == 1)
		{
			gradDegrees.erase(gradDegrees.begin() + index);
			gradVariables.erase(gradVariables.begin() + index);
		}
		else
		{
			gradCoeff *= (T)gradDegrees[index];
			gradDegrees[index] = gradDegrees[index] - 1;
		}
 		Monomial<T> gradMonomial(gradVariables, gradDegrees, gradCoeff, dim);
		return gradMonomial;
	}; // Get the first-order deriviative wrt variable[index]


	T EvaluateMonomial(std::vector<T> const& x)
	{
		T fValue = coeff;
		if (x.size() != dim)
		{
			std::cerr << "Error: input data dimension is different with the dimension of monomial!";
			return -1.0;
		}
		for (size_t i = 0; i < degrees.size(); i++)
		{
			if (degrees[i] >= 1)
				fValue *= pow( x[variables[i]], degrees[i]);
		}
		return fValue;
	};
	
	size_t GetDegree()
	{
		// deg is equal to sum of all the variable's degrees
		if (deg == -1)
		{
			// have not caculated before.
			deg = 0;
			for (size_t i = 0; i < degrees.size(); i++)
				deg += degrees[i];
		}
		return deg;
	};

	size_t DisplayMonomial()
	{
		std::cout << coeff;
		for (size_t i = 0; i < degrees.size(); i++)
			std::cout << "*x_" << variables[i] << "^" << degrees[i];
		// std::cout << std::endl;
		return 0;
	};


	T GetCoeff(){ return coeff; };
	size_t GetDim(){ return dim; };
	size_t GetVariable(size_t index){ return variables[index]; };
	size_t GetNumVariable(){ return numVariable; };

private:
	std::vector<size_t> variables;  // variable index, starting from 0
	std::vector<size_t> degrees; // variable degree, minimum is 1
	T coeff; // coeffecient of monomial
	size_t deg; // degree of monomial
	size_t dim; // dimension of monomial
	size_t numVariable;
};

template <class T>
class Polynomial
{
public:
	Polynomial(){};
	Polynomial(std::string filename)
	{
		std::ifstream polynomialFile(filename, std::ios_base::in);
		if (!polynomialFile.is_open())
			std::cerr << "Can't open data file" << std::endl;
		std::string type;
		std::string line;
		T coeff;
		size_t numVariable;
		std::vector<size_t> inputVariables(MAX_MONOMIAL, 0);
		std::vector<size_t> inputDegrees(MAX_MONOMIAL, 0);

		while (std::getline(polynomialFile, line))
		{
			std::istringstream in(line);
			in >> type;
			if (type == "monomial")
			{
				in >> coeff >> numVariable;
				inputVariables.resize(numVariable);
				inputDegrees.resize(numVariable);
				for (size_t i = 0; i < numVariable; i++)
				{
					in >> inputVariables[i] >> inputDegrees[i];
				}
				Monomial<T> tempMonomial(inputVariables, inputDegrees, coeff, dim);
				monomials.push_back(tempMonomial);
			}
			else if (type == "degree")
			{
				in >> deg;
			}
			else if (type == "dimension")
			{
				in >> dim;
			}
			else if (type == "constant")
			{
				in >> constant;
			}

		}
		monomialNum = monomials.size();
	};	
	Polynomial(std::vector< Monomial<T>> & inputMonomials, size_t inputDeg, size_t inputDim)
	{ 
		monomials = inputMonomials; 
		deg = inputDeg;
		dim = inputDim;
		monomialNum = inputMonomials.size();
		constant = 0.0;
	};
	~Polynomial(){};
	
	T EvaluatePolynomial(std::vector<T> const& x)
	{
		T fValue = constant;
		// typename std::vector<Monomial<T>>::iterator iter;
		// auto iter;
		for (auto iter = monomials.begin(); iter != monomials.end(); ++iter)
		{
			fValue += iter->EvaluateMonomial(x);
		}
		return fValue;
	};

	size_t GradientPolynomial(std::vector<T> const& x, std::vector<T>& g)
	{
		g.assign(dim, T(0.0));
		size_t k = 0;
		// for (typename std::vector<Monomial<T>>::iterator iter = monomials.begin(); iter != monomials.end(); ++iter)
		for (auto iter = monomials.begin(); iter != monomials.end(); ++iter)
		{
			k++;
			for (size_t i = 0; i < iter->GetNumVariable(); i++)
			{
				g[iter->GetVariable(i)] += iter->GradientMonomial(i).EvaluateMonomial(x);
			}
		}
		return 0;
	};

	size_t PlusPolynomial(Polynomial<T> inputPolynomial)
	{
		if (dim != inputPolynomial.getDim())
			std::cerr << "Dimension of two polynomials are not equal!";
		deg = fmin(deg, inputPolynomial.getDeg());
		constant = constant + inputPolynomial.getConstant();
		for (size_t i = 0; i < inputPolynomial.getMonomialNum(); i++)
			monomials.push_back(inputPolynomial.getMonomial(i));
		monomialNum = monomials.size();
		return 0;
	}
	
	size_t DisplayPolynomial()
	{
		// typename std::vector<Monomial<T>>::iterator iter;
		for (auto iter = monomials.begin() ; iter != monomials.end(); ++iter)
		{
			iter->DisplayMonomial();
			std::cout << "+";
		}
		std::cout << constant << std::endl;
		return 0;
	};

	size_t getDim(){ return dim; };
	size_t getDeg(){ return deg; };
	T getConstant(){ return constant; };
	Monomial<T> getMonomial(size_t index){ return monomials[index]; };
	size_t getMonomialNum(){ return monomialNum; };
private:
	std::vector< Monomial<T>> monomials;
	size_t deg;
	size_t dim;
	size_t monomialNum;
	T constant;
};

#endif
