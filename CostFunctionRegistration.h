//Author: Alexander G. Schwing
#include <vector>

#include "CostFunction.h"

#define NUM_POINTS 9;
#define NUM_EDGES 16;

template <class T>
class CostFunctionRegistration : public CostFunction<T> {
	size_t dim, nfactor;
	std::vector<T> nodeX;
	std::vector<T> nodeY;
	std::vector<T> nodeZ;
	std::vector<size_t> nodeInd1;
	std::vector<size_t> nodeInd2;
	std::vector<T> dist;
public:
	CostFunctionRegistration(){};
	CostFunctionRegistration(std::string filename) {
		std::ifstream polynomialFile(filename, std::ios_base::in);
		if (!polynomialFile.is_open())
			std::cerr << "Can't open data file" << std::endl;
		dim = NUM_POINTS;
		nfactor = NUM_EDGES;
		nodeX.resize(dim);
		nodeY.resize(dim);
		nodeZ.resize(dim);
		nodeInd1.resize(nfactor);
		nodeInd1 = { 1, 1, 2, 2, 2, 3, 3, 4, 4, 5, 5, 5, 6, 6, 7, 8 };
		nodeInd2.resize(nfactor);
		nodeInd2 = { 2, 4, 3, 4, 5, 5, 6, 5, 7, 6, 7, 8, 8, 9, 8, 9 };
		dist.resize(nfactor);
		dist = { 50.0, 50.0, 50.0, T(50.0*sqrt(2.0)), 50.0, T(50.0*sqrt(2.0)), 50.0, 50.0, 50.0, 50.0, T(50.0*sqrt(2.0)), 50.0, T(50.0*sqrt(2.0)), 50.0, 50.0, 50.0 };
		for (size_t k = 0; k < dim; k++)
		{
			polynomialFile >> nodeX[k] >> nodeY[k] >> nodeZ[k];
		}
	};

	CostFunctionRegistration(std::string filename, std::string edgefilename) {
		std::ifstream polynomialFile(filename, std::ios_base::in);
		if (!polynomialFile.is_open())
			std::cerr << "Can't open data file" << std::endl;
		std::ifstream edgeFile(edgefilename, std::ios_base::in);
		if (!edgeFile.is_open())
			std::cerr << "Can't open edge file" << std::endl;
		dim = 81;
		nfactor = 208;
		nodeX.assign(dim, T(0.0));
		nodeY.assign(dim, T(0.0));
		nodeZ.assign(dim, T(0.0));
		nodeInd1.assign(nfactor, size_t(0));
		nodeInd2.resize(nfactor, size_t(0));
		dist.resize(nfactor, T(1.0));

		for (size_t k = 0; k < nfactor; k++)
		{
			edgeFile >> nodeInd1[k] >> nodeInd2[k] >> dist[k];
		}
		for (size_t k = 0; k < dim; k++)
		{
			polynomialFile >> nodeX[k] >> nodeY[k] >> nodeZ[k];
		}
	};
	~CostFunctionRegistration() {};
	size_t Dimensions() { return dim; };
	T Evaluate(std::vector<T>& x, std::vector<T>& g, size_t) {
		T f = T(0.0);
		g.assign(dim, T(0.0));
		T temp = T(0.0);
		size_t ind1 = 0;
		size_t ind2 = 0;
		T diff1 = T(0.0); 
		T diff2 = T(0.0);
		T diff3 = T(0.0);
		for (size_t k = 0; k<nfactor; ++k) {
			temp = T(0.0);
			ind1 = nodeInd1[k] - 1;
			ind2 = nodeInd2[k] - 1;
			diff1 = x[ind1] * nodeX[ind1] - x[ind2] * nodeX[ind2];
			diff2 = x[ind1] * nodeY[ind1] - x[ind2] * nodeY[ind2];
			diff3 = x[ind1] * nodeZ[ind1] - x[ind2] * nodeZ[ind2];
			temp += diff1*diff1;
			temp += diff2*diff2;
			temp += diff3*diff3;
			temp -= dist[k]*dist[k];
			f += temp*temp;
			g[ind1] += 4 * (nodeX[ind1] * diff1 + nodeY[ind1] * diff2 + nodeZ[ind1] * diff3) * temp * 0.5 / T(nfactor);
			g[ind2] += -4 * (nodeX[ind2] * diff1 + nodeY[ind2] * diff2 + nodeZ[ind2] * diff3) * temp * 0.5 / T(nfactor);
		}
		f = 0.5 * f / T(nfactor);
		return f;
	}
	void IterationOutput(std::vector<T>&, std::vector<T>&, size_t) {}

};

template class CostFunctionRegistration<double>;
template class CostFunctionRegistration<float>;