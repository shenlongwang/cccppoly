//Author: Alexander G. Schwing
#include <vector>

#include "CostFunction.h"

template <class T>
class Optimizer {
private:
	int LS_init;
	int DDmethod;
	int cgUpdate;
	bool debug;
	bool keepBest;
	size_t MaxIterations;
public:
	Optimizer(size_t MaxIterations);
	~Optimizer();

	T Optimize(CostFunction<T>* CF, std::vector<T>* x_init);
};